# Lolnero node

A mobile node for Loloero.

<a href='https://play.google.com/store/apps/details?id=org.lolnero.node'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' height='80'/></a>


## How to use custom start up arguments

Sending the arguments to an unopened lolnero node app will cause `lolnerod` to use them on start up, for example:

`--add-exclusive-node 192.168.1.3`
